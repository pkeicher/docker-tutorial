FROM --platform=linux/amd64 python:3.11

RUN python -m pip install --upgrade pip
RUN python -m pip install requests

ADD script.py /analysis/script.py